from bacon.main import main


# A __name__ == '__main__' check omitted deliberately here since the original
# idea of the strip was to make the bacon appear on import

main()
